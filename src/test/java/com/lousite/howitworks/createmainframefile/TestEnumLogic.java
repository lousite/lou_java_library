package com.lousite.howitworks.createmainframefile;

import com.lousite.howitworks.workingwithenums.domain.PurchaseStatus;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@DisplayName("Pass enum values to our test method")
public class TestEnumLogic {

    @BeforeAll
    public static void setUp() {
        System.out.println("=====> Testing is beginning with the TestEnumLogic JUnit <=====");
    }
    @AfterAll
    public static void tearDown() {
        System.out.println("=====> Testing is complete for TestEnumLogic JUnit <=====");
    }

    /**
     *   Junit test - testEnumClassPurchaseStatus
     *   Description - Test PurchaseStatus ENUM, verify enum of BUY exist
     */
    @Test
    public void testEnumClassPurchaseStatus(){
        PurchaseStatus purchaseStatus = PurchaseStatus.BUY;
        System.out.println("PurchaseStatus enum is set a value: " + purchaseStatus);
        assertEquals(PurchaseStatus.valueOf("BUY"), purchaseStatus);
    }

    /**
     *   Junit test - testEnumClassPurchaseStatusIterateDefinitions
     *   Description - Test PurchaseStatus ENUM, verify enum of SELL exist
     */
    @Test
    public void testEnumClassPurchaseStatusIterateDefinitions() {
        PurchaseStatus purchaseStatus = PurchaseStatus.BUY;   // find a better way
        ArrayList<String> result = purchaseStatus.getListOfEnumNames();
        Iterator iterator = result.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
        assertTrue(result.contains("SELL"));
    }

    /**
     *   Junit test - testEnumClassPurchaseStatusIterateDefinitionsAll
     *   Description - Test PurchaseStatus ENUM, verify all 5 enums exist
     */
    @Test
    public void testEnumClassPurchaseStatusIterateDefinitionsAll() {
        PurchaseStatus[] purchaseStatuses = PurchaseStatus.values();
        Iterator<PurchaseStatus> iterator = Arrays.asList(purchaseStatuses).iterator();
        iterator.forEachRemaining(System.out::println);

        assertTrue(Arrays.asList(purchaseStatuses).contains(PurchaseStatus.STRONG_BUY));
        assertTrue(Arrays.asList(purchaseStatuses).contains(PurchaseStatus.BUY));
        assertTrue(Arrays.asList(purchaseStatuses).contains(PurchaseStatus.HOLD));
        assertTrue(Arrays.asList(purchaseStatuses).contains(PurchaseStatus.UNDERPERFORM));
        assertTrue(Arrays.asList(purchaseStatuses).contains(PurchaseStatus.SELL));
    }

    @DisplayName("Should pass non-null enum values as method parameters")
    @ParameterizedTest(name = "{index} => purchaseStatus=''{0}''")
    @EnumSource(PurchaseStatus.class)
    public void testEnumClassPurchaseStatus(PurchaseStatus purchaseStatus){
        assertNotNull(purchaseStatus);
    }
}
