package com.lousite.howitworks.createmainframefile;

public class TestPackDecimal {

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {

        String number = "154.10";

        System.out.println(" Covert " + number + " Using Packed Decimalm PackDec class ************");

        byte[] buffr1 = new byte[6];

        stringToPack(number, buffr1, 0, 6);

        System.out.println("Packed decimal Bytes ");
        printByte(buffr1);

        String encodedStr = new String(buffr1, "Cp037");
        System.out.println("Encoded String " + encodedStr);
        System.out.println("Encoded String Bytes ");
        printByte(encodedStr.getBytes());

        System.out.println("Encoded String Bytes2 Cp037 ");
        printByte(encodedStr.getBytes("Cp037"));

    }

    /**
     * Convenience method to convert a String to packed decimal. The packed
     * number is stored in an existing array.
     *
     * @param decimalValueAsString
     *            Number to be converted
     * @param bytearray
     *            Contains result
     * @param offset
     *            Location in array
     * @param maxLength
     *            Number of bytes of result
     * @throws Exception
     *             If result is larger than result length
     */
    public static void stringToPack(String decimalValueAsString, byte[] bytearray, int offset,
                                    int maxLength) throws Exception {
        byte[] decimalValuePacked = stringToPack(decimalValueAsString); /* get packed num of length 16 */
        int lengthOfPackedValue = decimalValuePacked.length;
        if(maxLength < lengthOfPackedValue) {
            if (decimalValuePacked[lengthOfPackedValue - maxLength - 1] != 0)
                throw new Exception("DecimalOverflowException : Number too big");
        }
        int i = offset + maxLength - 1;
        int j = lengthOfPackedValue - 1;
        for (int idx = 0; idx < maxLength; idx++) {
            bytearray[i] = decimalValuePacked[j];
            --i;
            --j;
        }
    }

    /**
     * Converts String to packed decimal number. Decimal points, commas and
     * spaces are ignored. Sign character is processed. Avoid multiple signs.
     * Characters other than digits are invalid and will cause DataException.
     * Comma, blank, period, dollar sign and plus are ignored. Scaling and
     * exponents are not valid.
     *
     * @param decimalValueAsString      String of number to convert
     * @return byte array of packed decimal, 16 long
     */
    public static byte[] stringToPack(String decimalValueAsString) throws Exception {
        return createPackedDecimalValue(decimalValueAsString, zeroOutPackedDecimalValue());
    }

    private static byte[] zeroOutPackedDecimalValue() {
        int zeroValue = 0;
        byte[] packedDecimalValue = new byte[16];
        for (int idxOfInputString = zeroValue; idxOfInputString < 16; idxOfInputString++) {
            // initialize final value to zeros (AKA X'00')
            packedDecimalValue[idxOfInputString] = (byte) zeroValue;
        }

        return packedDecimalValue;
    }

    private static byte[] createPackedDecimalValue(String decimalValueAsString, byte[] packedDecimalValue) throws Exception {
        int idxOfInputString = decimalValueAsString.length() - 1;; // index of input string
        int idxOfOutputValue = 15; // byte array index
        char charFromInputString;
        byte nibble;
        boolean nibble_ordinal = false;
        packedDecimalValue[idxOfOutputValue] = 12; // start with positive sign
        while (idxOfInputString > -1) {
            charFromInputString = decimalValueAsString.charAt(idxOfInputString);
            switch (charFromInputString) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    nibble = (byte) Character.getNumericValue(charFromInputString);
                    if (nibble_ordinal) {
                        packedDecimalValue[idxOfOutputValue] = (byte) (packedDecimalValue[idxOfOutputValue] | nibble);
                        nibble_ordinal ^= true;
                    } else {
                        packedDecimalValue[idxOfOutputValue] = (byte) (packedDecimalValue[idxOfOutputValue] | nibble << 4);
                        nibble_ordinal ^= true;
                        --idxOfOutputValue;
                    }
                    --idxOfInputString; // get the next character within the input string
                    break;
                case ',':
                case ' ':
                case '.':
                case '$':
                case '+':
                    --idxOfInputString; // get next character from input string
                    break;
                case '-':
                    // set last 4 bits 0000
                    packedDecimalValue[15] = (byte) (packedDecimalValue[15] & 0xf0);
                    packedDecimalValue[15] = (byte) (packedDecimalValue[15] | 0x0d);
                    --idxOfInputString; // get next char
                    break;
                default:
                    throw new Exception("Data Exception : Invalid decimal digit: "
                            + charFromInputString);
            }
        }
        return (packedDecimalValue);
    }

    public static void printByte(byte[] inputByteArray) {
        if (inputByteArray != null) {
            for (int i = 0; i < inputByteArray.length; i++) {
                System.out.print(inputByteArray[i] + " ");
            }
            System.out.println();
        }
    }

}
