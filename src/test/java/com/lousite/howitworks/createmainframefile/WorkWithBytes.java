package com.lousite.howitworks.createmainframefile;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class WorkWithBytes {
    public static void main(String[] args) {
        System.out.println("======================================================");
        System.out.println("SignPackedDecimal value: " + createSignPackedDecimalValue("1234567890"));

        System.out.println("======================================================");
        System.out.println("SignPackedDecimal value: " + createSignPackedDecimalValue("2022119"));
    }

    private static String createSignPackedDecimalValue(String inputNumericValue) {
        String signPackedDecimalValue = inputNumericValue;

        // Step 1 - Determine if input is negative or positive

        // Step 2 - Ensure we are only dealing with numeric characters

        // Step 3 - Reduce input to requested size

        // Step 4 - Move the value to an integer
        int intValueOfInput = Integer.valueOf(inputNumericValue);
        System.out.println("Input value (unsigned) as binary ===>" + Integer.toBinaryString(intValueOfInput));
        System.out.println("Input value (unsigned) as hex ======>" + Integer.toHexString(intValueOfInput));

        // Step 5 -
        byte[] inputValueAsByteArrayUTF8 = inputNumericValue.getBytes(StandardCharsets.UTF_8);
        try {
            byte[] inputValueAsByteArray = inputNumericValue.getBytes("cp037");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        String holdValue = null;
        boolean skipHalfByteJoin = false;
        int arrayOfIntSize = inputValueAsByteArrayUTF8.length/2 + 1;
        int[] arrayOfInt = new int[arrayOfIntSize];
        arrayOfIntSize--;
        for(int idx = inputValueAsByteArrayUTF8.length - 1; idx >= 0; idx--) {
            int intValueOfByte = inputValueAsByteArrayUTF8[idx];
            System.out.println("Binary value of byte " + idx + "===> " + Integer.toBinaryString(intValueOfByte));
            String lowerBinaryStringValue = Integer.toBinaryString(intValueOfByte).substring(2,6);
            System.out.println("Last four bits of byte value (we need this half byte value) ===> " + lowerBinaryStringValue);
            if(idx == inputValueAsByteArrayUTF8.length - 1) {
                // Create last byte including the needed sign
                arrayOfInt[arrayOfIntSize] = Integer.parseInt(lowerBinaryStringValue.concat("1100"), 2);
                System.out.println(arrayOfInt[arrayOfIntSize]);
                arrayOfIntSize--;
                skipHalfByteJoin = true;
            } else if(skipHalfByteJoin) {
                holdValue = lowerBinaryStringValue;
                skipHalfByteJoin = false;
            } else {
                arrayOfInt[arrayOfIntSize] = Integer.parseInt(lowerBinaryStringValue.concat(holdValue), 2);
                System.out.println(arrayOfInt[arrayOfIntSize]);
                arrayOfIntSize--;
                skipHalfByteJoin = true;
            }
        }
        if(!skipHalfByteJoin) {
            arrayOfInt[arrayOfIntSize] = Integer.parseInt("0000".concat(holdValue), 2);
            System.out.println(arrayOfInt[arrayOfIntSize]);
        }

        // Step 6 - String the byte/integer values together as one String value
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderMFHexVisual = new StringBuilder();
//        char[] convertedToCharArray = new char[arrayOfInt.length];
        for(int idx = 0; idx < arrayOfInt.length; idx++) {
            stringBuilder.append((char)arrayOfInt[idx]);
            System.out.println("int value =(" + arrayOfInt[idx] + ") byte value =(" + Integer.toBinaryString((char)arrayOfInt[idx]) + ") char value =(" + (char)arrayOfInt[idx] + ") hex value =(" + Integer.toHexString(arrayOfInt[idx]) + ")");
            stringBuilderMFHexVisual.append(Integer.toHexString(arrayOfInt[idx]));
        }
        System.out.println("stringBuilder value =======> " + stringBuilder);
        System.out.println("Mainframe hex visual =======> X'" + stringBuilderMFHexVisual + "'");

        return stringBuilder.toString();
    }
}
