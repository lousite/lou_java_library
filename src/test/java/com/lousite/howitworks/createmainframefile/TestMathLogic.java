package com.lousite.howitworks.createmainframefile;

import com.lousite.howitworks.math.domain.CompoundedAmount;
import com.lousite.howitworks.math.domain.MortgageCalculator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.text.NumberFormat;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@DisplayName("Test math methods")
public class TestMathLogic {

    @BeforeAll
    public static void setUp() {
        System.out.println("=====> Testing is beginning with the TestMathLogic JUnit <=====");
    }
    @AfterAll
    public static void tearDown() {
        System.out.println("=====> Testing is complete for TestMathLogic JUnit <=====");
    }

    /**
     *   Junit test - testMortgageCalculator
     *   Description - Test MortgageCalculator, success scenario
     */
    @Test
    public void testMortgageCalculator(){
        MortgageCalculator mortgageCalculator = new MortgageCalculator();
        double monthlyPayment = mortgageCalculator.getMonthlyPayment(100000,
                mortgageCalculator.getMonthlyInterestRateFromAnnual((float) .04),
                mortgageCalculator.getTotalMonthsWithinYear(15));

        System.out.println("Monthly payment: " + NumberFormat.getCurrencyInstance().format(monthlyPayment));

        System.out.println("Total payback amount: " + NumberFormat.getCurrencyInstance().
                format(monthlyPayment * mortgageCalculator.getTotalMonthsWithinYear(15)));

        assertEquals("$739.69", NumberFormat.getCurrencyInstance().format(monthlyPayment));
    }

    /**
     *   Junit test - testGettingCompoundedValue
     *   Description - Test compound calculation, success scenario
     */
    @Test
    public void testGettingCompoundedValue() {
        CompoundedAmount compoundedAmount = new CompoundedAmount();
        System.out.println(compoundedAmount.getCompoundedValue(1000.00,1,5,4,2));

        assertEquals("$1,104.49", NumberFormat.getCurrencyInstance().format(compoundedAmount.getCompoundedValue(1000.00,1,5,4,2)));
    }

}
