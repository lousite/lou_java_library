package com.lousite.howitworks.math.execute;

import com.lousite.howitworks.math.domain.MortgageCalculator;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.NumberFormat;
import java.util.Scanner;

public class MortgageCalculatorScannerInterface {
    private static final int MONTHS_IN_A_YEAR = 12;
    private static final int VALUE_OF_ONE = 1;
    private static final float TEN_PERCENT_AS_FLOAT = (float) .01;

    public static void main(String[] args) {
        MortgageCalculator mortgageCalculator = new MortgageCalculator();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the principal amount (amount borrowing): ");  // ex: 100000
        double principalAmount = scanner.nextDouble();

        System.out.println("Enter the annual interest rate (ex: 4.25): ");  // ex: .04
        float annualInterestRate = mortgageCalculator.calculateAnnualInterestRate(scanner.nextFloat());

        System.out.println("Enter the term in years: ");  // ex: 15
        int termInYears = scanner.nextInt();

        scanner.close();

        double monthlyPayment = mortgageCalculator.getMonthlyPayment(principalAmount,
                mortgageCalculator.getMonthlyInterestRateFromAnnual(annualInterestRate),
                mortgageCalculator.getTotalMonthsWithinYear(termInYears));

        System.out.println("Monthly payment: " + NumberFormat.getCurrencyInstance().format(monthlyPayment));

        System.out.println("Total payback amount: " + NumberFormat.getCurrencyInstance().
                format(monthlyPayment * mortgageCalculator.getTotalMonthsWithinYear(termInYears)));
    }

}
