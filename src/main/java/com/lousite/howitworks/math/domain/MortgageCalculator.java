package com.lousite.howitworks.math.domain;

public class MortgageCalculator {
    private static final int MONTHS_IN_A_YEAR = 12;
    private static final int VALUE_OF_ONE = 1;
    private static final double TEN_PERCENT_AS_FLOAT = .01;

    public float calculateAnnualInterestRate(double interestRateAsWhole) {
        return (float) (interestRateAsWhole * TEN_PERCENT_AS_FLOAT);
    }

    public float getMonthlyInterestRateFromAnnual(float annualInterestRate) {
        return annualInterestRate / MONTHS_IN_A_YEAR;
    }

    public int getTotalMonthsWithinYear(int years) {
        return years * MONTHS_IN_A_YEAR;
    }
    public double getMonthlyPayment(double principalAmount, float monthlyInterestRate, int totalNumberOfMonthlyPayments) {
        return principalAmount * (
                (monthlyInterestRate * (Math.pow(VALUE_OF_ONE + monthlyInterestRate, totalNumberOfMonthlyPayments))) /
                        ((Math.pow(VALUE_OF_ONE + monthlyInterestRate, totalNumberOfMonthlyPayments)) - VALUE_OF_ONE)
        );
    }
}
