package com.lousite.howitworks.math.domain;

import java.math.BigDecimal;

public class CompoundedAmount {

    /*
        FinalAmount = StartingAmount(CompoundedRate + Yield/NumberOfTimesGivenWithinCompoundRate) ^Time*NumberOfTimesGivenWithinCompoundRate
        FinalAmount = 1000(1Year + 5%/4 quarterly)^2Years * 4 quarterly
        FinalAmount = 1000(1.0125)^8
        $1104.49 = 1000(1.1045)
     */
    public static Double getCompoundedValue(double startingAmount, int compoundedRate, int yield, int numberOfTimesWithinYear, int totalYears) {
        double x = compoundedRate + ((yield * .01)/numberOfTimesWithinYear);
        System.out.println("X value=" + x);
        double x2 = totalYears * numberOfTimesWithinYear;
        System.out.println("X2 value=" + x2);
        double x3 = Math.pow(x, x2);
        System.out.println("X3 value=" + x3);

        return startingAmount * x3;
    };
}
