package com.lousite.howitworks.workingwithenums.domain;

import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public enum PurchaseStatus {
    STRONG_BUY("Strong Buy"),
    BUY("Buy"),
    HOLD("Hold"),
    UNDERPERFORM("Underperform"),
    SELL("Sell");

    private String purchaseStatus;

    PurchaseStatus(String _purchaseStatus) {
        this.purchaseStatus = _purchaseStatus;
    }

    public String getPurchaseStatus() {
        return this.purchaseStatus;
    }

    public ArrayList<String> getListOfEnumNames() {
        PurchaseStatus[] purchaseStatuses = PurchaseStatus.values();
        ArrayList<String> stringValuesOfPurchaseStatus = new ArrayList<String>();
        for (PurchaseStatus purchaseStatus : purchaseStatuses) {
            stringValuesOfPurchaseStatus.add(purchaseStatus.toString());
        }
        return stringValuesOfPurchaseStatus;
    }
}
