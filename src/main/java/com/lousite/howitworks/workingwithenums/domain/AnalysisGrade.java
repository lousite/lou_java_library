package com.lousite.howitworks.workingwithenums.domain;

public enum AnalysisGrade {
    UPGRADE,
    MAINTAINS,
    DOWNGRADE;
}
