package com.lousite.howitworks.workingwithenums.execute;

import com.lousite.howitworks.workingwithenums.domain.AnalysisGrade;
import com.lousite.howitworks.workingwithenums.domain.PurchaseStatus;

public class ExamplesUsingEnums {

    public static void main(String[] args) {
        System.out.println("This is a " + AnalysisGrade.UPGRADE);
    }
}
