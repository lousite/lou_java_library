package com.lousite.howitworks.createmainframefile.domain;

public record ByteRecord(byte[] outputFileInBytes) {
}
