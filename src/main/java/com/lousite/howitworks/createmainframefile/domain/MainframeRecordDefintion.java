package com.lousite.howitworks.createmainframefile.domain;

public class MainframeRecordDefintion {
    private String picxAlphaNumeric;
    private String pic9999;
    private byte[] pics999;
    private byte[] pics9999v99;
    private String picxEnd;

    public MainframeRecordDefintion(String picxAlphaNumeric, String pic9999, byte[] pics999, byte[] pics9999v99, String picxEnd) {
        this.picxAlphaNumeric = picxAlphaNumeric;
        this.pic9999 = pic9999;
        this.pics999 = pics999;
        this.pics9999v99 = pics9999v99;
        this.picxEnd = picxEnd;
    }

    public String getPicxAlphaNumeric() {
        return picxAlphaNumeric;
    }

    public void setPicxAlphaNumeric(String picxAlphaNumeric) {
        this.picxAlphaNumeric = picxAlphaNumeric;
    }

    public String getPic9999() {
        return pic9999;
    }

    public void setPic9999(String pic9999) {
        this.pic9999 = pic9999;
    }

    public byte[] getPics999() {
        return pics999;
    }

    public void setPics999(byte[] pics999) {
        this.pics999 = pics999;
    }

    public byte[] getPics9999v99() {
        return pics9999v99;
    }

    public void setPics9999v99(byte[] pics9999v99) {
        this.pics9999v99 = pics9999v99;
    }

    public String getPicxEnd() {
        return picxEnd;
    }

    public void setPicxEnd(String picxEnd) {
        this.picxEnd = picxEnd;
    }

}
