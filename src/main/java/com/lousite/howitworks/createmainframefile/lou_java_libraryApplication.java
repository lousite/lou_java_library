package com.lousite.howitworks.createmainframefile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class lou_java_libraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(lou_java_libraryApplication.class, args);
	}

}
