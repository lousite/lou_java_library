package com.lousite.howitworks.createmainframefile.execute;

import java.nio.charset.StandardCharsets;

public class ExampleOfSettingBitsInByte {
    private static final byte[] HEX_ARRAY = "0123456789ABCDEF".getBytes(StandardCharsets.US_ASCII);

    public static void main(String[] args) {
        int byteValue = 43;
        int newByteValue;
        newByteValue = ((byteValue & 0x0F) <<4 | (byteValue & 0x0F) >>4);
        System.out.println("Initial value: " + byteValue);
        System.out.println("Converting integer "+byteValue+" to Hex String: "+Integer.toHexString(byteValue));
        System.out.println("Converting integer "+byteValue+" to Hex String: "+Integer.toBinaryString(byteValue));

        System.out.println("New value: " + newByteValue);
        System.out.println("Converting integer "+newByteValue+" to Hex String: "+Integer.toHexString(newByteValue));
        System.out.println("Converting integer "+newByteValue+" to Hex String: "+Integer.toBinaryString(newByteValue));
    }

}
