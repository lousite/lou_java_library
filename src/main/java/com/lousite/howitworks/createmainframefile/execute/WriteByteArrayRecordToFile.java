package com.lousite.howitworks.createmainframefile.execute;

import com.lousite.howitworks.createmainframefile.domain.ByteRecord;
import com.lousite.howitworks.createmainframefile.domain.MainframeRecordDefintion;

import java.io.*;

public class WriteByteArrayRecordToFile {
    private static final String FILE_NAME1 = "C:\\Users\\louis\\LouStuff\\dev\\intellij-workspace\\lou_java_library\\src\\test\\resources\\testfilehardcoded.txt";
    private static final String FILE_NAME2 = "C:\\Users\\louis\\LouStuff\\dev\\intellij-workspace\\lou_java_library\\src\\test\\resources\\testfile.txt";
//    private static final String CHARACTER_SET = "IBM01140";
//    private static final String CHARACTER_SET = "UTF-8";
    private static final String CHARACTER_SET = "Cp1047";
//    private static final String CHARACTER_SET = "ASCII";
//    private static final String CHARACTER_SET = "Cp1047";  //IBM


    public static void main(String[] args) {
        hcValuesWriteToFile();
        generateMainframeFile();
    }

    private static void hcValuesWriteToFile() {
        byte[] client = { (byte) 0xF2, (byte) 0xF2, (byte) 0xF2, (byte) 0xF0 };
        byte[] formId = { (byte) 0xD3, (byte) 0xE3, (byte) 0xD9 };
        byte[] b1 = { (byte) 0xF0, (byte) 0xF0, (byte) 0xC0 };
        byte[] b2 = { (byte) 0x00, (byte) 0x00, (byte) 0x49, (byte) 0x9C };
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byteArrayOutputStream.write(client);
            byteArrayOutputStream.write(formId);
            byteArrayOutputStream.write(b1);
            byteArrayOutputStream.write(b2);
            byte testRecord[] = byteArrayOutputStream.toByteArray();

            ByteRecord byteRecord = new ByteRecord(testRecord);
            writeBytesToFile(FILE_NAME1, testRecord);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void generateMainframeFile() {
        byte[] PICS999 = { (byte) 0xF7, (byte) 0xF8, (byte) 0xC9 };
        byte[] PICS9999V99 = { (byte) 0x04, (byte) 0x56, (byte) 0x78, (byte) 0x9C };
        MainframeRecordDefintion mainframeRecordDefintion = new MainframeRecordDefintion("This an alphanumeric aka pic x value", "6789", PICS999, PICS9999V99, "END");

        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byteArrayOutputStream.write(mainframeRecordDefintion.getPicxAlphaNumeric().getBytes(CHARACTER_SET));
            byteArrayOutputStream.write(mainframeRecordDefintion.getPic9999().getBytes(CHARACTER_SET));
            byteArrayOutputStream.write(mainframeRecordDefintion.getPics999());
            byteArrayOutputStream.write(mainframeRecordDefintion.getPics9999v99());
            byteArrayOutputStream.write(mainframeRecordDefintion.getPicxEnd().getBytes(CHARACTER_SET));
            byte mainframeRecord[] = byteArrayOutputStream.toByteArray();

            ByteRecord byteRecord = new ByteRecord(mainframeRecord);
            writeBytesToFile(FILE_NAME2, mainframeRecord);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void writeBytesToFile(String fileOutputName, byte[] bytes) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(fileOutputName)) {
            fileOutputStream.write(bytes);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
